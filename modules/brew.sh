#!/usr/bin/env bash
set -euo pipefail

done_file=~/.$(basename "${BASH_SOURCE[0]}").done

[ -f done_file ] && return

xcode-select --install || true

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

brew_shellenv

if ! ([[ -e ~/.zprofile ]] && grep -q "brew shellenv" ~/.zprofile); then
    echo "eval \"\$(${HOMEBREW_PREFIX}/bin/brew shellenv)\"" >> "${HOME}/.zprofile"
fi

touch $done_file


