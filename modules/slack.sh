#!/usr/bin/env bash
set -euo pipefail

done_file=~/.$(basename "${BASH_SOURCE[0]}").done
[ -f "$done_file" ] && return

brew_cask_install slack

touch "$done_file"
