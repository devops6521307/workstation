#!/usr/bin/env bash
set -euo pipefail

done_file=~/.$(basename "${BASH_SOURCE[0]}").done
[ -f "$done_file" ] && return

brew install mysql@8.0 && brew link --force mysql@8.0

touch "$done_file"