#!/usr/bin/env bash 

get_team() {
	if [[ -z "${team}" ]]; then
    PS3="Which team are you in? "
    select team in frontend backend devops
    do
      break
    done
  fi
}

function brew_shellenv() {
  # It will export env variable: HOMEBREW_PREFIX, HOMEBREW_CELLAR, HOMEBREW_REPOSITORY, HOMEBREW_SHELLENV_PREFIX
  # It will add path: $PATH, $MANPATH, $INFOPATH
  if is_apple_silicon; then
      eval "$(/opt/homebrew/bin/brew shellenv)"
  else
      eval "$(/usr/local/bin/brew shellenv)"
  fi
}

brew_cask_install() {
  if (($# == 0)); then
    echo "Usage: brew_cask_install app"
    echo "       brew_cask_install app [artifacts]"
    echo ""
    echo "e.g. brew_cask_install notion"
    echo '     brew_cask_install intune-company-portal "Company Portal.app"'
    echo '     brew_cask_install microsoft-office "Microsoft Excel.app" "Microsoft Outlook.app" "Microsoft PowerPoint.app" "Microsoft Word.app" "OneDrive.app"'

    exit 0
  fi

  app=$1

  app_name="$(brew info --cask "$app" | grep -E '\(App\)' | awk 'BEGIN{OFS=FS} {NF--; print}')"

  if [[ ! -d "/Application/$app_name" ]]; then
    brew install --cask "$app"
  else
    echo "$app is already installed, skip it."
  fi
}