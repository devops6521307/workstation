#!/usr/bin/env bash 

echo Welcome to install.sh

root_dir="$(cd "$(dirname "$0")" && pwd)"

modules_dir="${root_dir}/modules"

install_dir="${root_dir}/installs"

## 加载公共函数

source "${root_dir}/helper.sh"

: "${team:=}"2

# 先安装公用的软件

# # shellcheck source=modules/macos.sh
# source "${modules_dir}/macos.sh"
# shellcheck source=modules/brew.sh
source "${modules_dir}/brew.sh"
# # shellcheck source=modules/mas.sh
# source "${modules_dir}/mas.sh"
# shellcheck source=modules/slack.sh
source "${modules_dir}/slack.sh"
# # shellcheck source=modules/wechat.sh
# source "${modules_dir}/wechat.sh"
# # shellcheck source=modules/iterm2.sh
# source "${modules_dir}/iterm2.sh"
# # shellcheck source=modules/vscode.sh
# source "${modules_dir}/vscode.sh"

get_team

case $team in

	backend )
		source "${install_dir}/install.backend.sh"
		;;
	front   )
		source "${install_dir}/install.front.sh"
		;;
	develop )
		source "${install_dir}/install.develop.sh"
		;;
esac
