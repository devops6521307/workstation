#!/usr/bin/env bash 

pwd="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

modules_dir="${pwd}/../modules"

source "${modules_dir}/mysql.sh"